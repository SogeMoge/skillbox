FROM node:latest

ENV HTTPS_PROXY=http://10.1.40.3:8080/
ENV HTTP_PROXY=http://10.1.40.3:8080/
RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

COPY . /app


#RUn yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
